﻿

namespace Paises
{
    using System;
    using System.Windows.Forms;
    using Paises.Modelos;
    using System.Collections.Generic;
    using GMap.NET;

    public partial class FormNavegar : Form
    {
        public FormNavegar(List<Pais> Paises)
        {
            InitializeComponent();

            InitCombos(Paises);

        }



        /// <summary>
        /// Inicia Combos e outros intens
        /// </summary>
        /// <param name="Paises"></param>
        private void InitCombos(List<Pais> Paises)
        {


            // carrega os dados para combobox
            ComboBoxPaises.DataSource = Paises;

            //mostra apenas os dados name
            ComboBoxPaises.DisplayMember = "name";


            //Settings dos NumericUpdown
            NumericUpDownLatitude.DecimalPlaces = 10;
            NumericUpDownLatitude.Minimum = -150;
            NumericUpDownLatitude.Maximum = 150;


            NumericUpDownLongitude.DecimalPlaces = 10;
            NumericUpDownLongitude.Minimum = -200;
            NumericUpDownLongitude.Maximum = 200;


        }


        /// <summary>
        /// Ao selecionar paises
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {

            //pais escolhido consoante a selecção na combobox
            Pais paisEscolhido = ComboBoxPaises.SelectedItem as Pais;

            //Insere valores das coordenadas do pais escolhido
            NumericUpDownLatitude.Value = Convert.ToDecimal(paisEscolhido.LatLng[0]);
            NumericUpDownLongitude.Value = Convert.ToDecimal(paisEscolhido.LatLng[1]);


            try
            {
                //Ver mapa com as coordenadas do pais escolhido
                VerMapa(Convert.ToDouble(NumericUpDownLatitude.Value), Convert.ToDouble(NumericUpDownLongitude.Value));

            }

            catch
            {
                //Se existir problema ao visualizar mapa do pais escolhido, as coordenadas no mapa são 0,0
                VerMapa(0, 0);
            }

        }



        private void VerMapa(double latitude, double longitude)
        {
            //Definições de zoom

            gMapControlNavegar.MaxZoom = 18;
            gMapControlNavegar.MinZoom = 2;


            //Servidor com mapas
            gMapControlNavegar.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;



            try
            {    //Acesso ao servidor dos mapas e a chache para melhorar desempenho
                GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;

                // Mapa é mostrado consoante a latitude e a longitude exibida ou inserida                
                gMapControlNavegar.Position = new GMap.NET.PointLatLng(latitude, longitude);

                //zoom do pais incial
                gMapControlNavegar.Zoom = 6;

            }

            catch
            {   //**** ALterar para quando se desligar a internet!!! ou não se conseguir aceder ao servidor!!


                //Caso não se consiga aceder ao servidor é apresentado um mapa guardado na cache
                GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.CacheOnly;

                MessageBox.Show("Não foi possivel aceder ao servidor do Mapa.\n O Mapa terá pouca resolução e as funcionalidades de navegação são limitadas!!");

                //Definições do mapa a apresentar
                gMapControlNavegar.Position = new GMap.NET.PointLatLng(latitude, longitude);
                //gMapControlNavegar.Position = new GMap.NET.PointLatLng(0, 0);
                //gMapControlNavegar.MinZoom = 1;
                //gMapControlNavegar.MaxZoom = 1;
                //gMapControlNavegar.Zoom = 1;

            }

        }

        /// <summary>
        /// Ver Mapa consoante a cidade inserida e o pais escolhido
        /// </summary>
        /// <param name="cidade"></param>
        private void VerMapa(string cidade, string pais)
        {
            try
            {
                GMaps.Instance.Mode = AccessMode.ServerAndCache;
            }

            catch
            {
                GMaps.Instance.Mode = AccessMode.CacheOnly;
            }

            //Mapa com indicação apenas de cidade introduzida na textbox
            gMapControlNavegar.SetPositionByKeywords(cidade + pais);


            gMapControlNavegar.Zoom = 10;

        }


        /// <summary>
        /// Ao carregar no botão o mapa apresenta o local das coordenadas inseridas nas NumericUpDown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCoordenadas_Click(object sender, EventArgs e)
        {

            VerMapa(Convert.ToDouble(NumericUpDownLatitude.Value), Convert.ToDouble(NumericUpDownLongitude.Value));

        }


        /// <summary>
        /// Botão para aceder no mapa à cidade introduzida
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCidades_Click(object sender, EventArgs e)
        {
            Pais paisEscolhido = ComboBoxPaises.SelectedItem as Pais;

            VerMapa(TextBoxCidade.Text, null);
        }
       

        /// <summary>
        /// Botão para aceder à cidade no país seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCidadeEPais_Click(object sender, EventArgs e)
        {
            VerMapa(TextBoxCidade.Text, ComboBoxPaises.Text);
        }


        private void ButtonSair_Click_1(object sender, EventArgs e)
        {
            Close();
        }

       
    }
}
