﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paises.Modelos
{
    public class Pais
    {
        //Propriedade adicionada para podermos trabalhar com Id dos Paises nas tabelas da base de dados
        public int IdPais { get; set; }

        public string Name { get; set; }
        public List<string> TopLevelDomain { get; set; }
        public string Alpha3Code { get; set; }
        public string Capital { get; set; }

        public string Region { get; set; }
        //public string subregion { get; set; }
        public int Population { get; set; }
        public List<double> LatLng { get; set; }


        //Alterado para string 
        public string Area { get; set; }


        //public List<string> timezones { get; set; }
        //public List<string> borders { get; set; }

        public List<Currency> Currencies { get; set; }
        //public List<Language> languages { get; set; }
        //public Translations translations { get; set; }
        public string Flag { get; set; }








    }
}
