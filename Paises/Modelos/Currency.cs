﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Paises.Modelos
{
    public class Currency : Pais
    {

        public string Code { get; set; }
        public string Name { get; set; }
        public string Symbol { get; set; }

        
    }
}
