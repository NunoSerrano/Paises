﻿
namespace Paises
{
    using Paises.Modelos;
    using Paises.Servicos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using ImageMagick;

    public partial class FormPaises : Form
    {

        #region atributos

        private List<Pais> PaisesInfo;

        private NetworkService netWorkService;

        private ApiService apiService;

        private DialogService dialogService;

        private DataService dataService;

        public FormNavegar formularioNavegar;


        #endregion



        public FormPaises()
        {
            InitializeComponent();
            netWorkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            PaisesInfo = new List<Pais>();
            LoadInfo();


        }


        #region API

        /// <summary>
        /// Carregar Informação dos Países
        /// </summary>
        private async void LoadInfo()
        {
            bool load;

            LabelResultado.Text = "A atualizar informação...";


            //testa conexão a internet através do método CheckConnection na classe netWorkService
            var connection = netWorkService.CheckConnection();


            // Caso não tenha sucesso
            if (!connection.IsSucess)
            {

                LoadLocalPaisesInfos();
                load = false;
                MessageBox.Show(connection.Message);

                ButtonNavegarMapa.Enabled = false;

            }

            else
            {
                await LoadApiInfo();
                load = true;


                GravarBandeirasNoDisco();
                ConverterImagensBandeiras();
            }



            // Se a lista não tem qq informação, retorna mensagem...
            
            if (PaisesInfo.Count == 0)
            {
                LabelResultado.Text = "Não há ligação à Internet" + Environment.NewLine +
                    " e não foram previamente carregadas as as informações dos países" + Environment.NewLine +
                    "Tente mais tarde!";

                LabelStatus.Text = "Primeira inicialização, deverá ter ligação à Internet";

                return;
            }

            else
            {
                //Iniciar combos
                InitCombo();
               

            }


            LabelResultado.Text = "Informação atualizada...";

            if (load)
            {
                LabelStatus.Text = string.Format("Informaçao carregada da internet em {0:F}", DateTime.Now);
            }

            else
            {
                LabelStatus.Text = string.Format("Informações dos paises carregadas da Base de Dados.\n Mapas dos paises não disponíveis OFFLINE!");
            }



            ProgressBar1.Value = 300;

        }


        /// <summary>
        /// Receber informação da Base de Dados
        /// </summary>
        private void LoadLocalPaisesInfos()
        {


            PaisesInfo = dataService.GetPaises();


        }



        /// <summary>
        /// Carrega informação através da API
        /// </summary>
        /// <returns></returns>
        private async Task LoadApiInfo()

        {
            // este método tem de ser async porque chama outro que é async


            ProgressBar1.Value = 30;

            //Envia o endereço da Api e o endereço do controlador para o método GetInfo na classe apiService
            var response = await apiService.GetInfo("https://restcountries.eu", "/rest/v2/all");

            PaisesInfo = (List<Pais>)response.Result;


            dataService.DeleteData();
            dataService.SaveData(PaisesInfo);

        }


        #endregion



        #region Vistas e Combos


        /// <summary>
        /// Mostra os nomes dos paises na ComboBox
        /// </summary>
        private void InitCombo()
        {
            // carrega os dados para combobox
            ComboBoxPaises.DataSource = PaisesInfo;

            //mostra apenas os dados name
            ComboBoxPaises.DisplayMember = "name";

        }

        

        /// <summary>
        /// Inicia as ListViews
        /// </summary>
        private void InitListView()
        {
            ListViewMoedas.Clear();
            ListViewInternet.Clear();


            // PROPRIEDADES
            ListViewMoedas.View = View.Details;
            ListViewInternet.View = View.Details;


            //COLUNAS
            ListViewMoedas.Columns.Add("MOEDA");
            ListViewMoedas.Columns.Add("SIMBOLO");
            ListViewMoedas.Columns.Add("CÓDIGO");

            ListViewInternet.Columns.Add("DOMINIO INTERNET");



            //AutoResize
            for (int idx = 0; idx <= 2; idx++)
            {
                ListViewMoedas.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);

            }

            //ListViewMoedas.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            ListViewInternet.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);


        }


        /// <summary>
        /// Adiciona e actualiza informação as ListViews
        /// </summary>
        /// <param name="PaisEscolhido"></param>
        private void ActualizaListViews(Pais PaisEscolhido)
        {

            InitListView();

            //condição para fazer testes pois os valores das moedas e dominio não estão na BD


            //Moedas
            try
            {
                foreach (var moedas in PaisEscolhido.Currencies)

                {
                    ListViewItem item;

                    item = ListViewMoedas.Items.Add(moedas.Name);
                    item.SubItems.Add(moedas.Symbol);
                    item.SubItems.Add(moedas.Code);

                }
            }

            catch
            {
                MessageBox.Show("Moedas não carregadas da BD");
            }


            try
            {
                //Dominio de Internet

                foreach (var dom in PaisEscolhido.TopLevelDomain)
                {
                    ListViewInternet.Items.Add(dom);
                }
            }

            catch
            {
                MessageBox.Show("Dominios não carregados da BD");

            }
        }

        #endregion


        # region Botões e escolhas

        /// <summary>
        /// Altera a escolha dos paises na ComboBox e altera as views
        /// </summary>
        /// 
        private void ComboBoxPaises_SelectedIndexChanged(object sender, EventArgs e)
        {

            // Pais escolhido na combobox
            Pais paisEscolhido = ComboBoxPaises.SelectedItem as Pais;


            //Informação do pais a carregar nas labels
            LabelAlphaCode3.Text = paisEscolhido.Alpha3Code;
            LabelRegiao.Text = paisEscolhido.Region;
            LabelCapital.Text = paisEscolhido.Capital;
            LabelPopulacao.Text = paisEscolhido.Population.ToString();
            LabelArea.Text = paisEscolhido.Area;

            //Tentativa de carregar coordenadas do pais
            try
            {
                LabelLatitude.Text = paisEscolhido.LatLng[0].ToString();
                LabelLongitude.Text = paisEscolhido.LatLng[1].ToString();
            }


            // se não existirem coordenadas
            catch
            {
                LabelLatitude.Text = "N/D".ToString();
                LabelLongitude.Text = "N/D".ToString();
            }


            // Ver bandeiras na PictureBox
            try
            {
                PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name + ".png");
            }
            
            //Se não existir bandeira 
            catch
            {
               // Tenta de novo
                if (PaisesInfo[0].Name == paisEscolhido.Name)
                {
                    PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name + ".png");
                }

                //mostra imagem de não disponível
                else
                {
                    PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens Alternativas\\unavailable.jpg");
                }
            }

           

            //Mostrar mapa do pais escolhido
            Mapa(paisEscolhido);

            // actualiza informação dos paises
            ActualizaListViews(paisEscolhido);

        }



        private void ButtonSair_Click(object sender, EventArgs e)
        {
            Close();
        }


        

        #endregion

        #region Bandeiras e Mapas


        /// <summary>
        /// Aceder a API e Gravar Bandeiras no disco
        /// </summary>
        private void GravarBandeirasNoDisco()
        {
            //Criar pasta Imagens
            if (!Directory.Exists("Imagens"))
            {
                Directory.CreateDirectory("Imagens");
            }

            try
            {
                foreach (Pais pais in PaisesInfo)
                {
                    string caminhoImagem = Application.StartupPath + "\\Imagens\\" + pais.Name + ".svg";

                    //Download bandeiras
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(pais.Flag, caminhoImagem);
                    }

                    ProgressBar1.Value += 1;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Erro! Não gravou bandeiras");
            }
        }


        /// <summary>
        /// Conversão dos ficheiros SVG para PNG para mostrar Bandeira
        /// </summary>
        /// <param name="paisEscolhido"></param>
        private void ConverterImagensBandeiras()
        {
            
                foreach (Pais pais in PaisesInfo)
                {
                    string caminhoImagemSVG = Application.StartupPath + "\\Imagens\\" + pais.Name + ".svg";
                    string caminhoImagemPng = Application.StartupPath + "\\Imagens\\" + pais.Name + ".png";


                    // Settings
                    MagickReadSettings settings = new MagickReadSettings();

                    //  Imagem a converter deverá ter o tamanho de 150x100
                    settings.Width = 300;
                    settings.Height = 200;


                    // Crear imagem
                    MagickImage imagemPng = new MagickImage(caminhoImagemSVG, settings);

                    // Convrter em formato png
                    imagemPng.Format = MagickFormat.Png;

                    // Grava imagem convertida
                    imagemPng.Write(caminhoImagemPng);
                }

                
        }



        /// <summary>
        /// Converte as imagens SVG em PNG
        /// </summary>
        /// <param name="paisEscolhido"></param>
        private void ConverterImagensBandeiras(Pais paisEscolhido)
        {

            try
            {
                foreach (Pais pais in PaisesInfo)
                {
                    string caminhoImagemSVG = Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name+ ".svg";
                    string caminhoImagemPng = Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name + ".png";


                    // Settings
                    MagickReadSettings settings = new MagickReadSettings();

                    //  Imagem a converter deverá ter o tamanho de 150x100
                    settings.Width = 300;
                    settings.Height = 200;


                    // Crear imagem
                    MagickImage imagemPng = new MagickImage(caminhoImagemSVG, settings);

                    // Convrter em formato png
                    imagemPng.Format = MagickFormat.Png;

                    // Grava imagem convertida
                    imagemPng.Write(caminhoImagemPng);
                }



                PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name + ".png");

                //Image imagemBandeira = Image.FromFile(caminhoImagemPng);

            }

            catch
            {
                if (PaisesInfo[0].Name == paisEscolhido.Name)
                {
                    PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens\\" + paisEscolhido.Name + ".png");
                }


                else
                {
                    PictureBoxBandeiras.Image = Image.FromFile(Application.StartupPath + "\\Imagens Alternativas\\unavailable.jpg");
                }
            }


        }




        /// <summary>
        /// Mostrar Mapa do pais escolhido
        /// </summary>
        /// <param name="paisEscolhido"></param>
        private void Mapa(Pais paisEscolhido)
        {
            gMapControl1.Enabled = false;
            //**Definições de zoom
            // Zoom Inicial
            gMapControl1.Zoom = 6;
            gMapControl1.MaxZoom = 20;
            gMapControl1.MinZoom = 3;


            //Servidor com mapas, com possibilidade de escolha
            gMapControl1.MapProvider = GMap.NET.MapProviders.GoogleMapProvider.Instance;



            try
            {    //Acesso ao servidor e a chache para apresentar os mapas
                GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;



                // Mapa é mostrado consoante a latitude e a longitude de cada pais               
                gMapControl1.Position = new GMap.NET.PointLatLng(paisEscolhido.LatLng[0], paisEscolhido.LatLng[1]);

                

                //Zoom do mapa consoante area do pais
                double area= double.Parse(paisEscolhido.Area, System.Globalization.CultureInfo.InvariantCulture);

                if (area == 0)
                {
                    gMapControl1.Zoom = 6;
                }

                
                else if (area < 100000)
                {
                    gMapControl1.Zoom = 7;
                }
               

                else if (Convert.ToDouble(area) <= 50000)
                {
                    gMapControl1.Zoom = 6;
                }

                else if (Convert.ToDouble(area) <= 200000)
                {
                    gMapControl1.Zoom = 5;
                }

                //else if (Convert.ToDouble(area) <= 500000)
                //{
                //    gMapControl1.Zoom = 6;
                //}

                else if (Convert.ToDouble(area) <= 5000000)
                {
                    gMapControl1.Zoom = 5;
                }



                else
                {
                    gMapControl1.Zoom =4;
                }

            }

            catch
            {
                GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.CacheOnly;
                gMapControl1.Position = new GMap.NET.PointLatLng(0, 0);
                gMapControl1.MinZoom = 1;
                gMapControl1.Zoom = 1;
            }

        }



        #endregion


        /// <summary>
        /// Creditos
        /// </summary>  
        private void creditosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Paises V2.0\n Autor: Nuno Serrano\n Data da versão: 10.12.2017", "CREDITOS");
        }


        /// <summary>
        ///  Botão para aceder a navegação do mapa
        /// </summary>
        private void ButtonNavegarMapa_Click(object sender, EventArgs e)
        {
            FormNavegar formularioNavegar = new FormNavegar(PaisesInfo);
            formularioNavegar.Show();
        }
    }



}



