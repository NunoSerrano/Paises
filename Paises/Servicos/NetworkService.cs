﻿

namespace Paises.Servicos
{
    using Modelos;
    using System.Net;

    public class NetworkService
    {

        /// <summary>
        /// Verifica se existe ligação à Internet
        /// </summary>        
        public Response CheckConnection ()
        {
            var client = new WebClient();

            try
            {
                //testar ligação à internet, pingando o servidor da google
                using (client.OpenRead("http://clients3.google.com/generate_204"))
                {
                    return new Response
                    {
                        IsSucess = true,
                    };
                }
               
            }

            //Se não existir ligação devolve um objeto tipo response e retorna uma mensagem
            catch
            {
                return new Response
                {
                    IsSucess = false,
                    Message = "Não tem ligação à Internet"
                };
            }

        }
    }
}
