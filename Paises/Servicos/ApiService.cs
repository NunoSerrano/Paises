﻿

namespace Paises.Servicos
{
    using Newtonsoft.Json;
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ApiService
    {

        /// <summary>
        /// Vai buscar as infos dos países. Tarefa assincrona que no fim devolve um objecto do tipo response
        /// </summary>
        /// <param name="urlbase"></param>
        /// <param name="controller"></param>
        /// <returns></returns>
        public async Task<Response> GetInfo(string urlbase, string controller)
        {
            try
            {
                // LIGAÇÃO EXTERNA via http
                var client = new HttpClient();

                //endereço API
                client.BaseAddress = new Uri(urlbase);


                //endereço controlador -> pasta onde está o contrololador dentro da API 
                // Get da API
                var response = await client.GetAsync(controller);

                // Carrega os resultados no formato string para a variável result (objecto)
                var result = await response.Content.ReadAsStringAsync();


                // confirmação
                // se correr mal
                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSucess = false,
                        Message = result,
                    };

                }
                
                //Carrega a variavel com uma lista de dados do tipo Pais que foram resebidos pela API via Json
                var infos = JsonConvert.DeserializeObject<List<Pais>>(result);

                return new Response
                {
                    IsSucess = true,
                    Result = infos,
                };
            }


            catch (Exception ex)
            {
                return new Response
                {
                    IsSucess = false,
                Message=ex.Message
                };
            }
        }



    }
}
