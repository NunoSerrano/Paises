﻿


namespace Paises.Servicos
{

    using System.Windows.Forms;

    public class DialogService
    {
        /// <summary>
        /// Mostrar uma MessageBox com titulo e mensagem
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        public void ShowMessage(string title, string message)
        {
             MessageBox.Show(message, title);
        }
    }
}
