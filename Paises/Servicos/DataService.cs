﻿
namespace Paises.Servicos
{
    using Paises.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    public class DataService
    {
        
        private SQLiteConnection connection;

        private SQLiteCommand command;

        private DialogService dialogService;


        /// <summary>
        /// Criar tabelas da Base de Dados
        /// </summary>
        public DataService()
        {
            dialogService = new DialogService();

            if (!Directory.Exists("Data"))
            {
                Directory.CreateDirectory("Data");
            }

            //Local onde vai guardar o ficheiro Paises.sqlite
            var path = @"Data\Paises.sqlite";

            try
            {
                //abre abase de dados ou cria se não existir
                connection = new SQLiteConnection("Data Source =" + path);
                connection.Open();



                //Lianguagem SQL

                //CRIAR TABELA PAISES
                string sqlcommand
                    = "Create table if not exists Paises(IdPais Integer PRIMARY KEY, Alpha3Code Nchar(3), Name Varchar(50), Capital Varchar(50), Region Varchar(50), Population Integer, Area VarChar(20))";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();


                //CRIAR TABELA CURRENCY

                sqlcommand
                    = "Create table if not exists Currencies(IdCurrency Integer PRIMARY KEY AUTOINCREMENT, Code NChar(3), Name VarChar(20), Symbol Varchar(50), IdPais Integer, FOREIGN KEY(IdPais) REFERENCES Paises(IdPais)  )";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();


                //CRIAR TABELA DOMINIOS

                sqlcommand
                    = "Create table if not exists Domains(IdDomain Integer PRIMARY KEY AUTOINCREMENT, TopLevelDomain nchar(5), IdPais Integer, FOREIGN KEY(IdPais) REFERENCES Paises(IdPais)  )";

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery();

                


            }


            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
            }



        }


        /// <summary>
        /// Gravar informação dos Paises na Base de Dados
        /// </summary>

        public void SaveData(List<Pais> Paises)
        {
            

            try
            {
                int idPais = 0;


                //TABELA PAISES
                foreach (var pais in Paises)
                {



                    string sql = string.Format("insert into Paises (IdPais, Alpha3Code, Name, Capital, Region, Population, Area) " +
                        "values (@idPais, @alpha3Code, @name, @capital, @region, @population, @area)");



                    //MessageBox.Show(idPais+" "+pais.Capital+" "+pais.Alpha3Code);

                    command = new SQLiteCommand(sql, connection);
                    command.Parameters.AddWithValue("@idPais", idPais);
                    command.Parameters.AddWithValue("@alpha3Code", pais.Alpha3Code);
                    command.Parameters.AddWithValue("@name", pais.Name);
                    command.Parameters.AddWithValue("@capital", pais.Capital);
                    command.Parameters.AddWithValue("@region", pais.Region);
                    command.Parameters.AddWithValue("@population", pais.Population);
                    command.Parameters.AddWithValue("@area", pais.Area);

                    idPais++;


                    //Tratamento dos parametros do tipo string que não estão preenchidos na API

                    if (!string.IsNullOrEmpty(pais.Alpha3Code))
                    {
                        command.Parameters.AddWithValue("@alpha3Code", pais.Alpha3Code);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@alpha3Code", " N/D");
                    }

                    if (!string.IsNullOrEmpty(pais.Name)) 
                        
                    {
                        command.Parameters.AddWithValue("@name", pais.Name);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@name", "N/D");
                    }


                    if (!string.IsNullOrEmpty(pais.Capital))
                    {
                        command.Parameters.AddWithValue("@capital", pais.Capital);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@capital", "N/D");
                    }

                    if (!string.IsNullOrEmpty(pais.Region))
                    {
                        command.Parameters.AddWithValue("@region", pais.Region);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@region", "N/D");
                    }


                    if (!string.IsNullOrEmpty(pais.Area))
                    {
                        command.Parameters.AddWithValue("@area", pais.Area);
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@area", "N/D");
                    }



                    // Executa os comandos
                    command.ExecuteNonQuery();



                    foreach (var moeda in pais.Currencies)

                    {
                        sql = string.Format("insert into Currencies (Code, Name, Symbol, idPais) values (@code, @name, @symbol, @idPais)");

                        command = new SQLiteCommand(sql, connection);

                        command.Parameters.AddWithValue("@code", moeda.Code);
                       command.Parameters.AddWithValue("@name", moeda.Name);
                        command.Parameters.AddWithValue("@symbol", moeda.Symbol);
                        command.Parameters.AddWithValue("@idPais", idPais);

                       // Tratamento dos parametros do tipo string que não estão preenchidos na API
                        if (string.IsNullOrEmpty(moeda.Code))
                        {
                            command.Parameters.AddWithValue("@code", " N/D");
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@code", moeda.Code);

                        }

                        if (string.IsNullOrEmpty(moeda.Name))


                        {
                            command.Parameters.AddWithValue("@name", "N/D");

                        }
                        else
                        {
                            command.Parameters.AddWithValue("@name", moeda.Name);
                        }


                        if (string.IsNullOrEmpty(moeda.Symbol))
                        {
                            command.Parameters.AddWithValue("@symbol", "N/D");

                        }
                        else
                        {
                            command.Parameters.AddWithValue("@symbol", moeda.Symbol);
                        }

                        // Executa os comandos
                        command.ExecuteNonQuery();
                    }

                    
                    //Inserir dados na tabela
                    foreach (var TopLevelDomain in pais.TopLevelDomain)

                    {
                      
                           sql = string.Format("insert into Domains (TopLevelDomain, idPais) values ( @TopLevelDomain, @idPais)");


                        command = new SQLiteCommand(sql, connection);

                        command.Parameters.AddWithValue("@TopLevelDomain", TopLevelDomain);
                        command.Parameters.AddWithValue("@idPais", idPais);



                        //Tratamento dos parametros do tipo string que não estão preenchidos na API

                        if (string.IsNullOrEmpty(TopLevelDomain))
                        {
                            command.Parameters.AddWithValue("@topLevelDomain", "N/D");
                        }

                        else
                        {
                            command.Parameters.AddWithValue("@topLevelDomain", TopLevelDomain);
                        }
                        //Executa os comandos
                        command.ExecuteNonQuery();

                    }

                    

                }


                connection.Close();
            }

            
            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
               
            }


        }
        

        /// <summary>
        /// Receber informação dos Paises, gravada na Base de Dados
        /// </summary>
        /// <returns></returns>
        public List<Pais> GetPaises()
        {

            
            List<Pais> paises = new List<Pais>();

            try
            {
                int counter = 0;

                string sql = "select  IdPais, Alpha3Code, Name, Capital, Region, Population, Area from Paises";
                command = new SQLiteCommand(sql, connection);

                //Lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    

                    paises.Add
                        (new Pais
                        {
                            IdPais = Convert.ToInt32(reader["Idpais"]),
                            Alpha3Code = (string)reader["Alpha3Code"],
                            Name = (string)reader["Name"],
                            Capital = (string)reader["Capital"],
                            Region = (string)reader["Region"],
                            Population = Convert.ToInt32(reader["Population"]),
                            Area = (string)reader["Area"],
                            Currencies = GetCurrencies(Convert.ToInt32(reader["idpais"])).ToList<Currency>(),
                            TopLevelDomain = GetDominios(Convert.ToInt32(reader["idpais"])).ToList<string>(),
                            

                        });

                    

                    counter++;

                }

                

                connection.Close();

                return paises;
                
            }

            catch (Exception e)
            {
                dialogService.ShowMessage("Erro", e.Message);
                return null;
            }
        }


        /// <summary>
        /// Receber informação da tabela Currencies na BD
        /// </summary>
        /// <param name="idpais"></param>
        /// <returns></returns>
        public List<Currency> GetCurrencies(int idpais)
        {
            List<Currency> currencies = new List<Currency>();

            try
            {

                string sql = "select Code, Name, Symbol from Currencies where idpais=" + idpais;

                command = new SQLiteCommand(sql, connection);

                //Lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    currencies.Add(new Currency
                    {
                        Code = (string)reader["code"],
                        Name = (string)reader["name"],
                        Symbol = (string)reader["symbol"],

                    });

                }


                return currencies;
            }

            catch (Exception e)
            {
                MessageBox.Show("Tabela Currencies, Pais nº " + idpais);
                dialogService.ShowMessage("Erro ao Ler dados da tabela Currencies", e.Message);
                return null;
            }
        }


        /// <summary>
        /// Receber informação da Tabela Domains na BD
        /// </summary>
        /// <param name="idpais"></param>
        /// <returns></returns>
        public List<string> GetDominios(int idpais)
        {
            List<string> dominios = new List<string>();

            try
            {

                string sql = "select TopLevelDomain from Domains where idpais=" + idpais;

                command = new SQLiteCommand(sql, connection);

                //Lê cada registo
                SQLiteDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    dominios.Add(reader["TopLevelDomain"].ToString());

                };


                return dominios;
            }

            catch (Exception e)
            {
                MessageBox.Show("Tabela Domains, Pais nº " + idpais);
                dialogService.ShowMessage("Erro ao Ler dados da tabela Domains", e.Message);
                return null;
            }
        }
        
        
        /// <summary>
        /// Apagar informação existente na Base de Dados
        /// </summary>
        public void DeleteData()
        {
            try
            {
                string sql = string.Format("delete from Paises; delete from Currencies; UPDATE SQLITE_SEQUENCE SET SEQ = 0");

                command = new SQLiteCommand(sql, connection);
                command.ExecuteNonQuery();
            }

            catch (Exception e)
            {
                dialogService.ShowMessage("Erro ao apagar dados das tabelas da Base de Dados para Update", e.Message);
            }
        }






    }
}
