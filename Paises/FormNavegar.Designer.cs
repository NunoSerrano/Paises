﻿namespace Paises
{
    partial class FormNavegar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNavegar));
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.NumericUpDownLatitude = new System.Windows.Forms.NumericUpDown();
            this.NumericUpDownLongitude = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.ButtonCoordenadas = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ButtonCidadeEPais = new System.Windows.Forms.Button();
            this.gMapControlNavegar = new GMap.NET.WindowsForms.GMapControl();
            this.TextBoxCidade = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonCidades = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLatitude)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLongitude)).BeginInit();
            this.SuspendLayout();
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(74, 243);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 18);
            this.label13.TabIndex = 71;
            this.label13.Text = "Latitude";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(60, 294);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(81, 18);
            this.label11.TabIndex = 70;
            this.label11.Text = "Longitude";
            // 
            // NumericUpDownLatitude
            // 
            this.NumericUpDownLatitude.Location = new System.Drawing.Point(165, 245);
            this.NumericUpDownLatitude.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.NumericUpDownLatitude.Minimum = new decimal(new int[] {
            150,
            0,
            0,
            -2147483648});
            this.NumericUpDownLatitude.Name = "NumericUpDownLatitude";
            this.NumericUpDownLatitude.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownLatitude.TabIndex = 68;
            // 
            // NumericUpDownLongitude
            // 
            this.NumericUpDownLongitude.Location = new System.Drawing.Point(165, 292);
            this.NumericUpDownLongitude.Name = "NumericUpDownLongitude";
            this.NumericUpDownLongitude.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownLongitude.TabIndex = 67;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(100, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 74;
            this.label1.Text = "País";
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(165, 51);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(325, 21);
            this.ComboBoxPaises.TabIndex = 73;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackColor = System.Drawing.Color.Transparent;
            this.ButtonSair.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSair.BackgroundImage")));
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonSair.Location = new System.Drawing.Point(34, 495);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(46, 42);
            this.ButtonSair.TabIndex = 81;
            this.ButtonSair.UseVisualStyleBackColor = false;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click_1);
            // 
            // ButtonCoordenadas
            // 
            this.ButtonCoordenadas.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCoordenadas.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonCoordenadas.BackgroundImage")));
            this.ButtonCoordenadas.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCoordenadas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCoordenadas.FlatAppearance.BorderSize = 0;
            this.ButtonCoordenadas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCoordenadas.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonCoordenadas.Location = new System.Drawing.Point(324, 254);
            this.ButtonCoordenadas.Name = "ButtonCoordenadas";
            this.ButtonCoordenadas.Size = new System.Drawing.Size(44, 37);
            this.ButtonCoordenadas.TabIndex = 72;
            this.ButtonCoordenadas.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.ButtonCoordenadas.UseVisualStyleBackColor = false;
            this.ButtonCoordenadas.Click += new System.EventHandler(this.ButtonCoordenadas_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(162, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(164, 18);
            this.label2.TabIndex = 85;
            this.label2.Text = "Inserir Cidade e Pais";
            // 
            // ButtonCidadeEPais
            // 
            this.ButtonCidadeEPais.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCidadeEPais.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonCidadeEPais.BackgroundImage")));
            this.ButtonCidadeEPais.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCidadeEPais.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCidadeEPais.FlatAppearance.BorderSize = 0;
            this.ButtonCidadeEPais.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCidadeEPais.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonCidadeEPais.Location = new System.Drawing.Point(382, 163);
            this.ButtonCidadeEPais.Name = "ButtonCidadeEPais";
            this.ButtonCidadeEPais.Size = new System.Drawing.Size(43, 36);
            this.ButtonCidadeEPais.TabIndex = 86;
            this.ButtonCidadeEPais.UseVisualStyleBackColor = false;
            this.ButtonCidadeEPais.Click += new System.EventHandler(this.ButtonCidadeEPais_Click);
            // 
            // gMapControlNavegar
            // 
            this.gMapControlNavegar.Bearing = 0F;
            this.gMapControlNavegar.CanDragMap = true;
            this.gMapControlNavegar.EmptyTileColor = System.Drawing.Color.Navy;
            this.gMapControlNavegar.GrayScaleMode = false;
            this.gMapControlNavegar.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapControlNavegar.LevelsKeepInMemmory = 5;
            this.gMapControlNavegar.Location = new System.Drawing.Point(566, 12);
            this.gMapControlNavegar.MarkersEnabled = true;
            this.gMapControlNavegar.MaxZoom = 2;
            this.gMapControlNavegar.MinZoom = 2;
            this.gMapControlNavegar.MouseWheelZoomEnabled = true;
            this.gMapControlNavegar.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapControlNavegar.Name = "gMapControlNavegar";
            this.gMapControlNavegar.NegativeMode = false;
            this.gMapControlNavegar.PolygonsEnabled = true;
            this.gMapControlNavegar.RetryLoadTile = 0;
            this.gMapControlNavegar.RoutesEnabled = true;
            this.gMapControlNavegar.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapControlNavegar.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapControlNavegar.ShowTileGridLines = false;
            this.gMapControlNavegar.Size = new System.Drawing.Size(875, 535);
            this.gMapControlNavegar.TabIndex = 75;
            this.gMapControlNavegar.Zoom = 0D;
            // 
            // TextBoxCidade
            // 
            this.TextBoxCidade.Location = new System.Drawing.Point(165, 113);
            this.TextBoxCidade.Name = "TextBoxCidade";
            this.TextBoxCidade.Size = new System.Drawing.Size(174, 20);
            this.TextBoxCidade.TabIndex = 87;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(81, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 18);
            this.label4.TabIndex = 88;
            this.label4.Text = "Cidade";
            // 
            // ButtonCidades
            // 
            this.ButtonCidades.BackColor = System.Drawing.Color.Transparent;
            this.ButtonCidades.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonCidades.BackgroundImage")));
            this.ButtonCidades.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonCidades.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCidades.FlatAppearance.BorderSize = 0;
            this.ButtonCidades.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonCidades.ForeColor = System.Drawing.Color.Transparent;
            this.ButtonCidades.Location = new System.Drawing.Point(382, 97);
            this.ButtonCidades.Name = "ButtonCidades";
            this.ButtonCidades.Size = new System.Drawing.Size(43, 36);
            this.ButtonCidades.TabIndex = 89;
            this.ButtonCidades.UseVisualStyleBackColor = false;
            this.ButtonCidades.Click += new System.EventHandler(this.ButtonCidades_Click);
            // 
            // FormNavegar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ClientSize = new System.Drawing.Size(1453, 563);
            this.Controls.Add(this.ButtonCidades);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextBoxCidade);
            this.Controls.Add(this.ButtonCidadeEPais);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.gMapControlNavegar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Controls.Add(this.ButtonCoordenadas);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.NumericUpDownLatitude);
            this.Controls.Add(this.NumericUpDownLongitude);
            this.Name = "FormNavegar";
            this.Text = "Mapa";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLatitude)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownLongitude)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown NumericUpDownLatitude;
        private System.Windows.Forms.NumericUpDown NumericUpDownLongitude;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Button ButtonCoordenadas;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ButtonCidadeEPais;
        private GMap.NET.WindowsForms.GMapControl gMapControlNavegar;
        private System.Windows.Forms.TextBox TextBoxCidade;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ButtonCidades;
    }
}