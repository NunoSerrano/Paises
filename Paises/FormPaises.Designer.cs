﻿namespace Paises
{
    partial class FormPaises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPaises));
            this.LabelResultado = new System.Windows.Forms.Label();
            this.ProgressBar1 = new System.Windows.Forms.ProgressBar();
            this.ButtonSair = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboBoxPaises = new System.Windows.Forms.ComboBox();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.LabelCapital = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LabelRegiao = new System.Windows.Forms.Label();
            this.LabelPopulacao = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ListViewInternet = new System.Windows.Forms.ListView();
            this.ListViewMoedas = new System.Windows.Forms.ListView();
            this.PictureBoxBandeiras = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.gMapControl1 = new GMap.NET.WindowsForms.GMapControl();
            this.label7 = new System.Windows.Forms.Label();
            this.LabelAlphaCode3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.LabelArea = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ButtonNavegarMapa = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.creditosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelLatitude = new System.Windows.Forms.Label();
            this.LabelLongitude = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeiras)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelResultado
            // 
            this.LabelResultado.AutoSize = true;
            this.LabelResultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelResultado.Location = new System.Drawing.Point(102, 473);
            this.LabelResultado.Name = "LabelResultado";
            this.LabelResultado.Size = new System.Drawing.Size(12, 16);
            this.LabelResultado.TabIndex = 22;
            this.LabelResultado.Text = "-";
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Location = new System.Drawing.Point(105, 533);
            this.ProgressBar1.Maximum = 300;
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(490, 23);
            this.ProgressBar1.TabIndex = 21;
            // 
            // ButtonSair
            // 
            this.ButtonSair.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonSair.BackgroundImage")));
            this.ButtonSair.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonSair.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonSair.FlatAppearance.BorderSize = 0;
            this.ButtonSair.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSair.Location = new System.Drawing.Point(1250, 547);
            this.ButtonSair.Name = "ButtonSair";
            this.ButtonSair.Size = new System.Drawing.Size(41, 38);
            this.ButtonSair.TabIndex = 14;
            this.ButtonSair.UseVisualStyleBackColor = true;
            this.ButtonSair.Click += new System.EventHandler(this.ButtonSair_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(691, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 13;
            this.label1.Text = "País";
            // 
            // ComboBoxPaises
            // 
            this.ComboBoxPaises.FormattingEnabled = true;
            this.ComboBoxPaises.Location = new System.Drawing.Point(765, 83);
            this.ComboBoxPaises.Name = "ComboBoxPaises";
            this.ComboBoxPaises.Size = new System.Drawing.Size(342, 21);
            this.ComboBoxPaises.TabIndex = 12;
            this.ComboBoxPaises.SelectedIndexChanged += new System.EventHandler(this.ComboBoxPaises_SelectedIndexChanged);
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(695, 543);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(37, 13);
            this.LabelStatus.TabIndex = 23;
            this.LabelStatus.Text = "Status";
            // 
            // LabelCapital
            // 
            this.LabelCapital.AutoSize = true;
            this.LabelCapital.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelCapital.Location = new System.Drawing.Point(201, 127);
            this.LabelCapital.Name = "LabelCapital";
            this.LabelCapital.Size = new System.Drawing.Size(13, 20);
            this.LabelCapital.TabIndex = 26;
            this.LabelCapital.Text = "-";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 18);
            this.label3.TabIndex = 27;
            this.label3.Text = "Capital do País";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(121, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 18);
            this.label5.TabIndex = 29;
            this.label5.Text = "Região";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(102, 327);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 18);
            this.label6.TabIndex = 30;
            this.label6.Text = "Moeda(s)";
            // 
            // LabelRegiao
            // 
            this.LabelRegiao.AutoSize = true;
            this.LabelRegiao.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelRegiao.Location = new System.Drawing.Point(201, 171);
            this.LabelRegiao.Name = "LabelRegiao";
            this.LabelRegiao.Size = new System.Drawing.Size(13, 20);
            this.LabelRegiao.TabIndex = 32;
            this.LabelRegiao.Text = "-";
            // 
            // LabelPopulacao
            // 
            this.LabelPopulacao.AutoSize = true;
            this.LabelPopulacao.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPopulacao.Location = new System.Drawing.Point(201, 261);
            this.LabelPopulacao.Name = "LabelPopulacao";
            this.LabelPopulacao.Size = new System.Drawing.Size(13, 20);
            this.LabelPopulacao.TabIndex = 36;
            this.LabelPopulacao.Text = "-";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(94, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 18);
            this.label8.TabIndex = 35;
            this.label8.Text = "População";
            // 
            // ListViewInternet
            // 
            this.ListViewInternet.BackColor = System.Drawing.Color.White;
            this.ListViewInternet.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ListViewInternet.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ListViewInternet.Location = new System.Drawing.Point(416, 363);
            this.ListViewInternet.Name = "ListViewInternet";
            this.ListViewInternet.Size = new System.Drawing.Size(179, 96);
            this.ListViewInternet.TabIndex = 37;
            this.ListViewInternet.UseCompatibleStateImageBehavior = false;
            this.ListViewInternet.View = System.Windows.Forms.View.Details;
            // 
            // ListViewMoedas
            // 
            this.ListViewMoedas.Location = new System.Drawing.Point(105, 363);
            this.ListViewMoedas.Name = "ListViewMoedas";
            this.ListViewMoedas.Size = new System.Drawing.Size(266, 96);
            this.ListViewMoedas.TabIndex = 40;
            this.ListViewMoedas.UseCompatibleStateImageBehavior = false;
            // 
            // PictureBoxBandeiras
            // 
            this.PictureBoxBandeiras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBoxBandeiras.ErrorImage = null;
            this.PictureBoxBandeiras.Location = new System.Drawing.Point(1141, 61);
            this.PictureBoxBandeiras.Name = "PictureBoxBandeiras";
            this.PictureBoxBandeiras.Size = new System.Drawing.Size(150, 100);
            this.PictureBoxBandeiras.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxBandeiras.TabIndex = 42;
            this.PictureBoxBandeiras.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(413, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 18);
            this.label2.TabIndex = 44;
            this.label2.Text = "Dominio(s) Internet";
            // 
            // gMapControl1
            // 
            this.gMapControl1.Bearing = 0F;
            this.gMapControl1.CanDragMap = true;
            this.gMapControl1.EmptyTileColor = System.Drawing.Color.AliceBlue;
            this.gMapControl1.GrayScaleMode = false;
            this.gMapControl1.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gMapControl1.LevelsKeepInMemmory = 5;
            this.gMapControl1.Location = new System.Drawing.Point(694, 123);
            this.gMapControl1.MarkersEnabled = true;
            this.gMapControl1.MaxZoom = 2;
            this.gMapControl1.MinZoom = 2;
            this.gMapControl1.MouseWheelZoomEnabled = true;
            this.gMapControl1.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gMapControl1.Name = "gMapControl1";
            this.gMapControl1.NegativeMode = false;
            this.gMapControl1.PolygonsEnabled = true;
            this.gMapControl1.RetryLoadTile = 0;
            this.gMapControl1.RoutesEnabled = true;
            this.gMapControl1.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gMapControl1.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gMapControl1.ShowTileGridLines = false;
            this.gMapControl1.Size = new System.Drawing.Size(597, 404);
            this.gMapControl1.TabIndex = 45;
            this.gMapControl1.Zoom = 0D;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(17, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 18);
            this.label7.TabIndex = 46;
            this.label7.Text = "Codigo (alphacode3)";
            // 
            // LabelAlphaCode3
            // 
            this.LabelAlphaCode3.AutoSize = true;
            this.LabelAlphaCode3.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelAlphaCode3.Location = new System.Drawing.Point(201, 83);
            this.LabelAlphaCode3.Name = "LabelAlphaCode3";
            this.LabelAlphaCode3.Size = new System.Drawing.Size(13, 20);
            this.LabelAlphaCode3.TabIndex = 47;
            this.LabelAlphaCode3.Text = "-";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(78, 219);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 18);
            this.label9.TabIndex = 50;
            this.label9.Text = "Area do País";
            // 
            // LabelArea
            // 
            this.LabelArea.AutoSize = true;
            this.LabelArea.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelArea.Location = new System.Drawing.Point(201, 218);
            this.LabelArea.Name = "LabelArea";
            this.LabelArea.Size = new System.Drawing.Size(13, 20);
            this.LabelArea.TabIndex = 49;
            this.LabelArea.Text = "-";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(413, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 18);
            this.label12.TabIndex = 54;
            this.label12.Text = "Latitude";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(399, 128);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 18);
            this.label10.TabIndex = 56;
            this.label10.Text = "Longitude";
            // 
            // ButtonNavegarMapa
            // 
            this.ButtonNavegarMapa.BackColor = System.Drawing.Color.Transparent;
            this.ButtonNavegarMapa.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ButtonNavegarMapa.BackgroundImage")));
            this.ButtonNavegarMapa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonNavegarMapa.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonNavegarMapa.FlatAppearance.BorderSize = 0;
            this.ButtonNavegarMapa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonNavegarMapa.Location = new System.Drawing.Point(573, 222);
            this.ButtonNavegarMapa.Name = "ButtonNavegarMapa";
            this.ButtonNavegarMapa.Size = new System.Drawing.Size(45, 43);
            this.ButtonNavegarMapa.TabIndex = 57;
            this.ButtonNavegarMapa.UseVisualStyleBackColor = false;
            this.ButtonNavegarMapa.Click += new System.EventHandler(this.ButtonNavegarMapa_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Black;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.creditosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1340, 27);
            this.menuStrip1.TabIndex = 58;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // creditosToolStripMenuItem
            // 
            this.creditosToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.creditosToolStripMenuItem.Font = new System.Drawing.Font("Maiandra GD", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.creditosToolStripMenuItem.Name = "creditosToolStripMenuItem";
            this.creditosToolStripMenuItem.Size = new System.Drawing.Size(87, 23);
            this.creditosToolStripMenuItem.Text = "Creditos";
            this.creditosToolStripMenuItem.Click += new System.EventHandler(this.creditosToolStripMenuItem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(399, 233);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 18);
            this.label4.TabIndex = 59;
            this.label4.Text = "Navegar no Mapa";
            // 
            // LabelLatitude
            // 
            this.LabelLatitude.AutoSize = true;
            this.LabelLatitude.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLatitude.Location = new System.Drawing.Point(489, 86);
            this.LabelLatitude.Name = "LabelLatitude";
            this.LabelLatitude.Size = new System.Drawing.Size(13, 20);
            this.LabelLatitude.TabIndex = 53;
            this.LabelLatitude.Text = "-";
            // 
            // LabelLongitude
            // 
            this.LabelLongitude.AutoSize = true;
            this.LabelLongitude.Font = new System.Drawing.Font("Arial Narrow", 11.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelLongitude.Location = new System.Drawing.Point(489, 126);
            this.LabelLongitude.Name = "LabelLongitude";
            this.LabelLongitude.Size = new System.Drawing.Size(13, 20);
            this.LabelLongitude.TabIndex = 55;
            this.LabelLongitude.Text = "-";
            // 
            // FormPaises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.ClientSize = new System.Drawing.Size(1340, 597);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ButtonNavegarMapa);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.LabelLongitude);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.LabelLatitude);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.LabelArea);
            this.Controls.Add(this.LabelAlphaCode3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PictureBoxBandeiras);
            this.Controls.Add(this.gMapControl1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ListViewMoedas);
            this.Controls.Add(this.ListViewInternet);
            this.Controls.Add(this.LabelPopulacao);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.LabelRegiao);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelCapital);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.LabelResultado);
            this.Controls.Add(this.ProgressBar1);
            this.Controls.Add(this.ButtonSair);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ComboBoxPaises);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FormPaises";
            this.Text = "Paises";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxBandeiras)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelResultado;
        private System.Windows.Forms.ProgressBar ProgressBar1;
        private System.Windows.Forms.Button ButtonSair;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox ComboBoxPaises;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label LabelCapital;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label LabelRegiao;
        private System.Windows.Forms.Label LabelPopulacao;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListView ListViewInternet;
        private System.Windows.Forms.ListView ListViewMoedas;
        private System.Windows.Forms.PictureBox PictureBoxBandeiras;
        private System.Windows.Forms.Label label2;
        private GMap.NET.WindowsForms.GMapControl gMapControl1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label LabelAlphaCode3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label LabelArea;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button ButtonNavegarMapa;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem creditosToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelLatitude;
        private System.Windows.Forms.Label LabelLongitude;
    }
}

